<!doctype html>
<html ng-app="app">
    <head>
        <?php include_once('inc/_tags.php') ?>
    </head>
    <body>
        <?php include_once('inc/_header.php') ?>

		<ng-view></ng-view>

        <?php include_once('inc/_footer.php') ?>
        <?php include_once('inc/_js.php') ?>
    </body>
</html>
