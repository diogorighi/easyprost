module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            css: {
                src: [
                    'extras/**/*.css',
                    'css/dev/_normalize.css',
                    'css/dev/_grid.css',
                    'css/dev/_base.css',
                    'css/dev/_modules.css',
                    'css/dev/_helpers.css'
                ],
                dest: 'css/app.css'
            },
            js: {
                options: {
                    separator: ";",
                    stripBanners: {
                        block: true,
                        line: true
                    },
                    // Replace all 'use strict' statements in the code with a single one at the top
                    // banner: "'use strict';\n",
                    process: function(src, filepath) {
                        return '// Source: ' + filepath + '\n' +
                        src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                    },
                },
                src: [
                    'js/dev/plugins.js',
                    'js/dev/main.js',
                ],
                dest: 'js/app.js'
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 0
            },
            minify: {
                src: 'css/app.css',
                dest: 'css/app.min.css'
            }
        },
        uglify: {
            js: {
                files: {
                    'js/app.min.js': ['js/app.js']
                }
            }
        },
        watch: {
            html: {
                files: ['**/*.php'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['js/dev/*.js', "extras/**/*.js"],
                tasks: ['js'],
                options: {
                    livereload: true,
                },
            },
            css: {
                files: ['css/dev/*.css', "extras/**/*.css"],
                tasks: ['css'],
                options: {
                    livereload: true,
                },
            },
        },
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('css', ["concat:css", "cssmin:minify"])
    grunt.registerTask('js', ["concat:js", "uglify:js"])
};