<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>EasyProst</title>

<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=640, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

<link rel="shortcut icon" href="img/icons/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="img/_icons/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/_icons/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/_icons/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/_icons/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="img/_icons/apple-touch-icon-152x152-precomposed.png">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/app.min.css">

<!--[if lt IE 9]>
	<script src="js/dev/html5shiv.js"></script>
<![endif]-->
