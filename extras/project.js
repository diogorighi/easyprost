var app = angular.module('app', ['ngRoute', 'ui.slider']);

app.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'template/login.html',
			controller  : 'loginCtrl'
		})
		.when('/cadastro', {
			templateUrl : 'template/cadastro.html'
		})
		.when('/cidade', {
			templateUrl : 'template/cidade.html'
		})
		.when('/acompanhantes', {
			templateUrl : 'template/acompanhantes.html',
			controller  : 'AppCtrl'
		})
		.when('/acompanhante/:id', {
			templateUrl : 'template/acompanhante.html',
			controller  : 'AcompanhanteCtrl'
		})
		.when('/mensagem/:id', {
			templateUrl : 'template/mensagem.html',
			controller  : 'MensagemCtrl'
		})
		.when('/enviada/:id', {
			templateUrl : 'template/enviada.html',
			controller  : 'MensagemCtrl'
		});
});

app.controller('AppCtrl', ['$scope', '$http',function ($scope, $http) {
  
	$http.get('teste.json').success(function (data) {
		$scope.acompanhantes = data.list[0].acompanhante;
	});

	$scope.filtro = false;
	$scope.filtroSexo = "F";
	$scope.filtroSeio = "";
	$scope.filtroExtras = [];
	$scope.filtroIdade = [18,55];
	$scope.filtroPreco = [10,5000];

	$scope.mudaFiltroSexo = function(sexo){
		$scope.filtroSexo = sexo;
	}

	$scope.mudaFiltroSeio = function(seio){
		$scope.filtroSeio = seio;
	}

	$scope.toggleFilter = function(){
		$scope.filtro = $scope.filtro === false ? true: false;
	}

	$scope.closeFilter = function(){
		$scope.filtro = false;
	}
	
	
	//filtro de extras
	$scope.addFiltroExtra = function(extra){
		if( $scope.containsObject(extra,$scope.filtroExtras) ) {
			var index = $scope.filtroExtras.indexOf(extra);
			$scope.filtroExtras.splice(index, 1);     
		} else {
			$scope.filtroExtras.push(extra);
		};
	}
	
	//filtro de extras
	$scope.filterExtras = function(item) {
		var qtdExtras = $scope.filtroExtras.length;
		var qtdTotal = 0;
		for (var i = 0; i < item.extras.length; i++) {
			if( $scope.containsObject(item.extras[i], $scope.filtroExtras) ) {
				qtdTotal = qtdTotal + 1;
			}
		}
		
	    if(qtdExtras == qtdTotal) {
	        return true;
	    }

	    return false;
	};
	
	//filtro de idade
	$scope.filterIdade = function(item) {
		
		if( (item.idade >= $scope.filtroIdade[0]) && item.idade <= $scope.filtroIdade[1]){
			return true
		}	

	    return false;
	};
	
	//filtro de preco
	$scope.filterPreco = function(item) {
		
		if( (item.preco >= $scope.filtroPreco[0]) && item.preco <= $scope.filtroPreco[1]){
			return true
		}	

	    return false;
	};
	
	//helpers
	$scope.containsObject = function(obj, list) {
	    var i;
	    for (i = 0; i < list.length; i++) {
	        if (angular.equals(list[i], obj)) {
	            return true;
	        }
	    }

	    return false;
	};

}]);

app.controller('AcompanhanteCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams	) {
	$http.get('teste.json').success(function (data) {
		$scope.acompanhantes = data.list[0].acompanhante;

		angular.forEach($scope.acompanhantes, function(acompanhante, index) {
			if(acompanhante.id == $routeParams.id){
				$scope.perfil = (acompanhante);
			}
	    });
	});
}]);

app.controller('MensagemCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams	) {
	$http.get('teste.json').success(function (data) {
		$scope.acompanhantes = data.list[0].acompanhante;

		angular.forEach($scope.acompanhantes, function(acompanhante, index) {
			if(acompanhante.id == $routeParams.id){
				$scope.perfil = (acompanhante);
			}
	    });
	});
}]);
