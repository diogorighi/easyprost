<!doctype html>
<html ng-app="app">
    <head>
        <?php include_once('inc/_tags.php') ?>
    </head>
    <body ng-controller="AppCtrl">
        <?php include_once('inc/_header.php') ?>


        <?php include_once('inc/_footer.php') ?>
        <?php include_once('inc/_js.php') ?>
        <script src="extras/project.js"></script>
        <script>
        	$(function(){
                $('.btn-filtrar').on('click',function(e){
                    e.preventDefault();
                    $('.filtro').fadeIn(300);
                })

                $('.btn-fechar').on('click',function(e){
                    e.preventDefault();
                    $('.filtro').fadeOut(300);
                })
        	})
        </script>
    </body>
</html>
