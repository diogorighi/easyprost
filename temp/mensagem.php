<!doctype html>
<html>
    <head>
        <?php include_once('inc/_tags.php') ?>
    </head>
    <body>
        <?php include_once('inc/_header.php') ?>
		
		<div class="container bg-cidade">
			<div class="content content-cidade">
		        <p><strong class="pink">De: </strong>Fulano</p>
		        <p><strong class="pink">Para: </strong>Mia</p>
				
				<p>Deixe sua mensagem:</p>

		        <form action="enviada.php" class="form-default">
		        	<textarea name="mensagem" id="mensagem" class="mensagem"></textarea>
		        	<div class="group-buttons">
			        	<a href="index.php" class="btn btn-pink transparente">Cancelar</a>
			        	<input type="submit" value="Enviar" class="btn btn-pink">
			        </div>
		        </form>
	        </div>	
		</div>

        <?php include_once('inc/_footer.php') ?>
        <?php include_once('inc/_js.php') ?>
    </body>
</html>
