<!doctype html>
<html>
    <head>
        <?php include_once('inc/_tags.php') ?>
    </head>
    <body>
        <?php include_once('inc/_header.php') ?>
		
		<div class="container bg-cidade">
	        <h1 class="title-cidade">EasyProst</h1>
			
			<div class="content content-cidade">
				<p>Sua mensagem foi enviada para <span class="pink">Mia</span> com sucesso! Em breve ela entrará em contato com você.</p>

				<a href="principal_angular.php" class="btn btn-green btn-full">OK</a>
	        </div>	
		</div>


        <?php include_once('inc/_footer.php') ?>
        <?php include_once('inc/_js.php') ?>
    </body>
</html>
