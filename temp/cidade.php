<!doctype html>
<html>
    <head>
        <?php include_once('inc/_tags.php') ?>
    </head>
    <body>
        <?php include_once('inc/_header.php') ?>
		
		<div class="container bg-cidade">
	        <h1 class="title-cidade">EasyProst</h1>
			
			<div class="content content-cidade">
		        <p>Escolha a cidade desejada</p>

		        <form action="principal_angular.php" class="form-default">
		        	<select name="cidades" class="select select-cidade">
						<option value="0">Selecione a cidade</option>
						<option value="natal">Natal</option>
						<option value="fortaleza">Fortaleza</option>
						<option value="recife">Recife</option>
					</select>
					<div class="group-buttons">
			        	<input type="submit" value="OK" class="btn btn-full btn-green">
			        </div>
		        	
		        </form>
	        </div>	
		</div>


        <?php include_once('inc/_footer.php') ?>
        <?php include_once('inc/_js.php') ?>
    </body>
</html>
